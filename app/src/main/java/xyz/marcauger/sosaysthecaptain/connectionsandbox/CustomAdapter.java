package xyz.marcauger.sosaysthecaptain.connectionsandbox;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class CustomAdapter extends ArrayAdapter<DataItem> {

    Context context;
    int layoutResourceId;
    List<DataItem> data = null;

    // Takes context, the cell layout, and list of DataItems
    public CustomAdapter(@NonNull Context context, int resource, @NonNull List<DataItem> objects) {
        super(context, resource, objects);

        this.layoutResourceId = resource;
        this.context = context;
        this.data = objects;
    }

    // Holds references to UI elements
    static class DataHolder {
        ImageView imageView;
        TextView titleTextView;
        TextView dateTextView;
    }

    // Given position in array, sets the relevant row
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        DataHolder holder = null;

        // First clause fires the for the first cell, second clause thereafter
        if (convertView == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);

            holder = new DataHolder();
            holder.imageView = (ImageView)convertView.findViewById(R.id.imageView);
            holder.titleTextView = (TextView)convertView.findViewById(R.id.titleTextView);
            holder.dateTextView = (TextView)convertView.findViewById(R.id.dateTextView);

            convertView.setTag(holder);
        } else {
            holder = (DataHolder)convertView.getTag();

        }

        // Set text and images based on position index
        DataItem dataItem = data.get(position);
        holder.titleTextView.setText(dataItem.title);
        holder.dateTextView.setText(dataItem.date);
        holder.imageView.setImageBitmap(dataItem.thumbnail);

        return convertView;
    }
}
