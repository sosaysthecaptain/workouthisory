package xyz.marcauger.sosaysthecaptain.connectionsandbox;

import android.graphics.Bitmap;

// Custom class for each List View cell, holding the relevant data
public class DataItem {
    String title;
    String date;
    String thumbnailURL;
    Bitmap thumbnail;

    public DataItem(String title, String date, String thumbnailURL, Bitmap thumbnail) {
        this.title = title;
        this.date = date;
        this.thumbnailURL = thumbnailURL;
        this.thumbnail = thumbnail;
    }
}
