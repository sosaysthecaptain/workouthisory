package xyz.marcauger.sosaysthecaptain.connectionsandbox;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.icu.text.SimpleDateFormat;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;


public class MainActivity extends AppCompatActivity {
    ListView mainListView;
    List<DataItem> lstData;
    int loadedImageCount = 0;           // images actually loaded
    int claimedImageCount = 0;          // images on which function has been called
    CustomAdapter customAdapter = null;


    public class DownloadNextBatchOfThumbnails extends AsyncTask<Void, Void, Void> {
        /*
        Gets next 20 unloaded images. Originally implemented to load images batchwise, and left
        to facilitate implementing this functionality in the future.

        Currently only used to load the first batch of thumbnails.
         */

        @Override
        protected Void doInBackground(Void... voids) {

            int batchSize = 10;
            int startPosition = loadedImageCount;
            int endPosition = startPosition + batchSize;

            for(int i = startPosition; i < endPosition; i++) {
                Bitmap bmp = null;
                String currentIndexUrlString = lstData.get(i).thumbnailURL;

                try {
                    URL currentIndexURL = new URL(currentIndexUrlString);
                    bmp = BitmapFactory.decodeStream(currentIndexURL.openConnection().getInputStream());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                lstData.get(i).thumbnail = bmp;
            }

            loadedImageCount += batchSize;

            return null;
        }
    }

    public class DownloadThumbnailsInBackground extends AsyncTask<Void, Void, Void> {
        /*
        After the first screenfull of thumbnails are downloaded, this downloads the rest of them
        asynchronously.
         */

        @Override
        protected Void doInBackground(Void... voids) {

            int startPoint = 10;

            for(int i = startPoint; i < lstData.size(); i++) {
                Bitmap bmp = null;
                String currentIndexUrlString = lstData.get(i).thumbnailURL;

                try {
                    URL currentIndexURL = new URL(currentIndexUrlString);
                    bmp = BitmapFactory.decodeStream(currentIndexURL.openConnection().getInputStream());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                lstData.get(i).thumbnail = bmp;
            }

            return null;
        }
    }

    public class DownloadTask extends AsyncTask<String, Void, String> {

        HttpURLConnection urlConnection;
        StringBuilder result = new StringBuilder();

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected String doInBackground(String... urls) {
            /*
            Main download task. Gets JSON, parses it, populates list of DataItems.
            Leaves bitmaps as null. Bitmaps are loaded separately and asynchronously to
            improve performance.

            A number of other loading schemes could be used instead--pull-for-more, batchwise
            loading when the user scrolls within a certain distance of the end of the list, etc.

            One could also load JSON chunkwise--I'm doing it all at once because (a) it doesn't take
            long, and (b) it gives the user the impression that everything loaded at once.
            */


            String[] apiURLs = {
                    "https://staging.myxfitness.com/api/v1/users/11/workout_history?page=1",
                    "https://staging.myxfitness.com/api/v1/users/11/workout_history?page=2",
                    "https://staging.myxfitness.com/api/v1/users/11/workout_history?page=3",
                    "https://staging.myxfitness.com/api/v1/users/11/workout_history?page=4",
                    "https://staging.myxfitness.com/api/v1/users/11/workout_history?page=5"};
            for (String url : apiURLs) {
                result = new StringBuilder();

                try {
                    URL myUrl = new URL(url);
                    //URL myUrl = new URL("https://staging.myxfitness.com/api/v1/users/11/workout_history?page=1");
                    urlConnection = (HttpURLConnection) myUrl.openConnection();

                    // Add authorization headers
                    urlConnection.setRequestProperty("Authorization",
                            "Basic ODQxNTEzMzFjNmZiNTBjNTNmZjFjMmJkMjFmZjJlNDY6");
                    urlConnection.setRequestProperty("Auth-Token", "faadfefe9e2a4ed99ebac690979d5fff");
                    urlConnection.setRequestProperty("Content-Type", "application/json");

                    // Log response code and message
                    int responseCode = urlConnection.getResponseCode();
                    String responseMessage = urlConnection.getResponseMessage();
                    Log.i("Debug resCode: ", Integer.toString(responseCode));
                    Log.i("Debug resMessage: ", responseMessage);

                    // Create stream, read it to response, line by line
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                } catch (Exception e) {
                    Log.i("Debug", "ERROR");
                    e.printStackTrace();
                }

                // Parse JSON
                JSONArray outputJSONArray = null;
                try {
                    outputJSONArray = new JSONArray(result.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Date formats, for use later
                SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat newFormat = new SimpleDateFormat("E, dd MMM yyyy");

                // Iterate through array, appending title, date, videoURL
                for (int i = 0; i < outputJSONArray.length(); i++) {
                    JSONObject entry = null;
                    String startedAt = null;
                    JSONObject videoObject = null;
                    String title = null;
                    String videoURL = null;
                    try {
                        entry = outputJSONArray.getJSONObject(i);
                        startedAt = entry.getString("started_at");
                        videoObject = entry.getJSONObject("video");
                        title = videoObject.getString("title");
                        videoURL = videoObject.getString("thumbnail_image");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    // format the date
                    Date parsedDate = null;
                    String outputDate = "";
                    try {
                        parsedDate = oldFormat.parse(startedAt);
                        outputDate = newFormat.format(parsedDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    // Get bitmap
                    URL newVideoURL = null;
                    Bitmap bmp = null;

                    // Create new DataItem, append it
                    DataItem newDataItem = new DataItem(title, outputDate, videoURL, bmp);
                    lstData.add(newDataItem);

                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            /*
            Runs in the background after the main download task, downloading the rest of the
            thumbnails without requiring the user to wait.
            */
            super.onPostExecute(s);

            //Download the rest of the thumbnails in the background
            DownloadThumbnailsInBackground backgroundDownloadTask = new DownloadThumbnailsInBackground();
            backgroundDownloadTask.execute();

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Instantiate lstData
        lstData = new ArrayList<>();

        // Run the download task. This will populate titles, dates, and videoURLs arrays
        DownloadTask task = new DownloadTask();
        String downloadTaskOutput = null;
        try {
            task.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        // Load the first batch of thumbnails before displaying anything
        DownloadNextBatchOfThumbnails newTask = new DownloadNextBatchOfThumbnails();
        try {
            newTask.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        // Setup the ListView
        mainListView = (ListView) findViewById(R.id.mainListView);
        customAdapter = new CustomAdapter(this, R.layout.layout_row, lstData);
        mainListView.setAdapter(customAdapter);


        /*
        These methods are relevant to loading on pull-for-more or on user scrolling to within
        a certain distance of the end of the list. I decided against implementing either of these,
        in favor of loading all thumbnails in the background.
        */

//        mainListView.setOnScrollListener(new AbsListView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//                //Log.i("Debug", "onScrollStateChanged firing. scrollState: " + scrollState);
//
//                if (scrollState == 1) {
//                    //Log.i("Debug", "TIME TO LOAD MORE");
//                }
//            }
//
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                /*
//                When user has scrolled to within 10 items of the last image loaded, load the next
//                batch of images.
//                */
//                int lastVisibleItem = firstVisibleItem + visibleItemCount;
//
//
//                if ((firstVisibleItem + 10) > claimedImageCount) {
//                    Log.i("Debug", "Time to load more images");
//                }
//
//
//            }
//        });
    }

}
